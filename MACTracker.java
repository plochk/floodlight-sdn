/* Listen for new MAC addresses and log them on stdout.
 * author: plochk
 */
package net.floodlightcontroller.mactracker;

import java.util.Collection;
import java.util.Map;

import org.openflow.protocol.OFMessage;
import org.openflow.protocol.OFType;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IOFMessageListener;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.core.IFloodlightProviderService;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.Set;

import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.util.MACAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MACTracker implements IOFMessageListener, IFloodlightModule {

	protected IFloodlightProviderService floodlightProvider;
	protected Set<Long> macAddresses;
	protected static Logger logger;
	
	@Override
	public String getName() {
		return MACTracker.class.getSimpleName();
	}

	@Override
	public boolean isCallbackOrderingPrereq(OFType type, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCallbackOrderingPostreq(OFType type, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleServices() {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public Map<Class<? extends IFloodlightService>, IFloodlightService> getServiceImpls() {
		// TODO Auto-generated method stub
		return null;
	}

	/* Tell module loader what we depend on */
	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
		Collection<Class<? extends IFloodlightService>> l = 
				new ArrayList<Class<? extends IFloodlightService>>();
		l.add(IFloodlightProviderService.class);
		return l;
	}

	@Override
	public void init(FloodlightModuleContext context)
			throws FloodlightModuleException {
		floodlightProvider = context.getServiceImpl(IFloodlightProviderService.class);
		macAddresses = new ConcurrentSkipListSet<Long>();
		logger = LoggerFactory.getLogger(MACTracker.class);
	}

	/* Tell Floodlight we want to listen to events for OFPT_PACKET_IN messages. */
	@Override
	public void startUp(FloodlightModuleContext context)
			throws FloodlightModuleException {
		floodlightProvider.addOFMessageListener(OFType.PACKET_IN, this);
	}

	/* Upon message receival */
	@Override
	public net.floodlightcontroller.core.IListener.Command receive(
			IOFSwitch sw, 
			OFMessage msg, 
			FloodlightContext cntx) {
		
		switch( msg.getType() ) 
		{
		case PACKET_IN:
			// pass to packet processing
			return processPacket(sw, msg, cntx);
			
		default: break;
		}
		
		return net.floodlightcontroller.core.IListener.Command.CONTINUE;
	}
	
	/* Packet Processing */
	public net.floodlightcontroller.core.IListener.Command processPacket(
			IOFSwitch sw, 
			OFMessage msg, 
			FloodlightContext cntx) {
		
		/* Retrieve the deserialized packet in message */
		Ethernet eth = IFloodlightProviderService.bcStore.get(
				cntx, IFloodlightProviderService.CONTEXT_PI_PAYLOAD);
		MACAddress sourceMAC = eth.getSourceMAC();
		
		/* Check if its a new MAC address and log it */
		if(!macAddresses.contains(sourceMAC.toLong())) {
			macAddresses.add(sourceMAC.toLong());
			logger.info("MAC Address: {} seen on switch: {}",
					    sourceMAC.toString(),
					    sw.getId());
		}
		
		return net.floodlightcontroller.core.IListener.Command.CONTINUE;
	}

}
