#!/usr/bin/env bash

sudo wireshark -i 'lo' -k -Y 'openflow_v1' &
sleep 1s
sudo mn --controller=remote,ip=localhost:6633 --switch=ovs
exit 0
